from ports import scan_ports
from subdomain import find_subdomain
from wafw00f.main import main
from get_ip import get_ip
from ip_range import add_ip_range
import time


def start():
    result = []
    host = str(input("enter host: "))
    default = int(input("enter number of ports: "))
    ports = scan_ports(host, default)
    print('ports///////////////////////////////////')
    for port in ports:
        print(port)
    print('domains///////////////////////////////////')
    domains = find_subdomain(host)
    for domain in domains:
        print(domain)
    print('WAF///////////////////////////////////')
    print(main(host))


def sub_and_ip(host):
    mount = set()
    domains = find_subdomain(host)
    for domain in domains:
        print(domain)
        ip = get_ip(domain)
        print(ip)
        add_ip_range(ip, mount)
    print(mount)
    print(len(mount))


if __name__ == '__main__':
    start = time.time()
    host = str(input("enter host: "))
    # start()
    sub_and_ip(host)
    end = time.time() - start
    print(end)
