import nmap3
def scan_ports(host, default=10):
    nmap = nmap3.Nmap()
    result = []
    dump = nmap.scan_top_ports(host, default=default)
    keys = list(dump.keys())
    ports = dump[keys[0]]['ports']
    for port in ports:
        if port['state'] == 'open':
            result.append(port)
    if result is None:
        return print(f"no opening ports from address :{host}")
    return result
