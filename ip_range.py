import re
import ipaddress


def add_ip_range(range_ip, mount):
    iprange = re.split(' . ', range_ip)

    start_ip = ipaddress.IPv4Address(iprange[0])
    end_ip = ipaddress.IPv4Address(iprange[1])

    for ip_int in range(int(start_ip), int(end_ip)):
        #print(ipaddress.IPv4Address(ip_int))
        mount.add(ipaddress.IPv4Address(ip_int))
    return mount

