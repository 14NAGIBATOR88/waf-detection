from knockpy import knockpy


def find_subdomain(host):
    params = {
        "no_local": True,
    }
    results = knockpy.Scanning.start(host, params)
    return(results)