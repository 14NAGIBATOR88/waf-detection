import socket
def get_ip(host):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(("whois.ripe.net", 43))

    #convert string to bytes, socket need bytes
    ip = socket.gethostbyname(host)
    s.send((ip + "\r\n").encode())

    #declares a bytes
    response = b""
    while True:
        data = s.recv(4096)
        response += data
        if not data:
            break
    s.close()

    #convert bytes to string
    result = response.decode().split('\n')
    inum = list(filter(lambda x: 'inetnum:        ' in x, result))[0].split('inetnum:        ')
    #inetnum =  result.split('inetnum:        ')
    return inum[1]

#print(get_ip("citilab.ru"))
